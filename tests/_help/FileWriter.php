<?php

namespace Avris\FunctionMock;

class FileWriter
{
    const FILE = __DIR__ . '/file.txt';

    const CONTENT = 'content';

    public function write()
    {
        return file_put_contents(self::FILE, self::CONTENT);
    }
}
