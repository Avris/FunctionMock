<?php
namespace Avris\FunctionMock;

use PHPUnit\Framework\TestCase;

class FunctionMockTest extends TestCase
{
    /** @var FileWriter */
    private $writer;

    protected function setUp()
    {
        $this->writer = new FileWriter();
        $this->removeFile();
    }

    protected function tearDown()
    {
        FunctionMock::clean();
    }

    public function testMock()
    {
        $mock = FunctionMock::create(__NAMESPACE__, 'file_put_contents', true);
        $this->doTestMockFirstInvocation($mock);
        $this->doTestMockSecondInvocation($mock);

        $mock = FunctionMock::create(__NAMESPACE__, 'file_put_contents', function ($filename, $content) {
            return $content;
        });
        $this->doTestMockAfterOverwrite($mock);

        FunctionMock::clean();
        $this->doTestMockAfterClean($mock);
        $this->removeFile();

        $mock = FunctionMock::create(__NAMESPACE__, 'file_put_contents', 'foo');
        $this->doTestMockAfterRedeclaration($mock);

        $this->assertSame(['\Avris\FunctionMock\file_put_contents' => $mock], FunctionMock::all());
    }

    private function doTestMockFirstInvocation(FunctionMock $mock)
    {
        $result = $this->writer->write();

        $this->assertFileNotExists(FileWriter::FILE);
        $this->assertSame([[FileWriter::FILE, FileWriter::CONTENT]], $mock->getInvocations());
        $this->assertSame(true, $result);

        $this->assertSame('\Avris\FunctionMock\file_put_contents', $mock->getFqfn());
        $this->assertSame(__NAMESPACE__, $mock->getNamespace());
        $this->assertSame('file_put_contents', $mock->getFunction());
        $this->assertSame(true, $mock->getReturn());
        $this->assertTrue($mock->isEnabled());
    }

    private function doTestMockSecondInvocation(FunctionMock $mock)
    {
        $result = $this->writer->write();

        $this->assertFileNotExists(FileWriter::FILE);
        $this->assertSame([
            [FileWriter::FILE, FileWriter::CONTENT],
            [FileWriter::FILE, FileWriter::CONTENT],
        ], $mock->getInvocations());
        $this->assertSame(true, $result);
    }

    private function doTestMockAfterOverwrite(FunctionMock $mock)
    {
        $result = $this->writer->write();

        $this->assertFileNotExists(FileWriter::FILE);
        $this->assertSame([[FileWriter::FILE, FileWriter::CONTENT]], $mock->getInvocations());
        $this->assertSame(FileWriter::CONTENT, $result);

        $this->assertSame('\Avris\FunctionMock\file_put_contents', $mock->getFqfn());
        $this->assertSame(__NAMESPACE__, $mock->getNamespace());
        $this->assertSame('file_put_contents', $mock->getFunction());
        $this->assertTrue(is_callable($mock->getReturn()));
        $this->assertTrue($mock->isEnabled());
    }

    private function doTestMockAfterClean(FunctionMock $mock)
    {
        $this->assertSame([], $mock->getInvocations());

        $this->writer->write();

        $this->assertFileExists(FileWriter::FILE);
        $this->assertSame([], $mock->getInvocations());
        $this->assertFalse($mock->isEnabled());
    }

    private function doTestMockAfterRedeclaration(FunctionMock $mock)
    {
        $result = $this->writer->write();

        $this->assertFileNotExists(FileWriter::FILE);
        $this->assertSame([[FileWriter::FILE, FileWriter::CONTENT]], $mock->getInvocations());
        $this->assertSame('foo', $result);

        $this->assertSame('\Avris\FunctionMock\file_put_contents', $mock->getFqfn());
        $this->assertSame(__NAMESPACE__, $mock->getNamespace());
        $this->assertSame('file_put_contents', $mock->getFunction());
        $this->assertSame('foo', $mock->getReturn());
        $this->assertTrue($mock->isEnabled());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage \Foo\bar is not mocked
     */
    public function testManualTriggerUndefined()
    {
        FunctionMock::trigger('\Foo\bar', []);
    }

    private function removeFile()
    {
        @unlink(FileWriter::FILE);
        $this->assertFileNotExists(FileWriter::FILE);
    }
}
