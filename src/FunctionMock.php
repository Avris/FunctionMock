<?php
namespace Avris\FunctionMock;

class FunctionMock
{
    const FUNC_TEMPLATE = <<<PHP
namespace %s {
    function %s() {
        return \Avris\FunctionMock\FunctionMock::trigger('%s', func_get_args());
    }
}
PHP;

    /** @var self[] */
    private static $instances = [];

    /** @var string */
    private $namespace;

    /** @var string */
    private $function;

    /** @var callable|mixed */
    private $return;

    /** @var bool */
    private $enabled = true;

    /** @var array[] */
    private $invocations = [];

    public static function create(string $namespace, string $function, $return)
    {
        $fqfn = self::buildFqfn($namespace, $function);

        return isset(self::$instances[$fqfn])
            ? self::overwrite(self::$instances[$fqfn], $return)
            : self::$instances[$fqfn] = new self($namespace, $function, $return);
    }

    private function __construct(string $namespace, string $function, $return)
    {
        eval(sprintf(self::FUNC_TEMPLATE, $namespace, $function, self::buildFqfn($namespace, $function)));

        $this->namespace = $namespace;
        $this->function = $function;
        $this->return = $return;
    }

    private static function overwrite(FunctionMock $instance, $return)
    {
        $instance->clearInvocations();
        $instance->enable();
        $instance->return = $return;

        return $instance;
    }

    private static function buildFqfn(string $namespace, string $function)
    {
        return sprintf('\\%s\\%s', $namespace, $function);
    }

    /**
     * @internal
     */
    public static function trigger(string $fqfn, array $args)
    {
        $instance = self::$instances[$fqfn] ?? null;

        if (!$instance) {
            throw new \InvalidArgumentException(sprintf('%s is not mocked', $fqfn));
        }

        if (!$instance->enabled) {
            return call_user_func_array('\\' . $instance->function, $args);
        }

        $instance->invocations[] = $args;

        return is_callable($instance->return)
            ? call_user_func_array($instance->return, $args)
            : $instance->return;
    }

    public function getFqfn(): string
    {
        return self::buildFqfn($this->namespace, $this->function);
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function getFunction(): string
    {
        return $this->function;
    }

    /**
     * @return callable|mixed
     */
    public function getReturn()
    {
        return $this->return;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function enable(): self
    {
        $this->enabled = true;

        return $this;
    }

    public function disable(): self
    {
        $this->enabled = false;

        return $this;
    }

    /**
     * @return array[]
     */
    public function getInvocations(): array
    {
        return $this->invocations;
    }

    public function clearInvocations(): self
    {
        $this->invocations = [];

        return $this;
    }

    public static function clean()
    {
        foreach (self::$instances as $instance) {
            $instance->disable();
            $instance->clearInvocations();
        }
    }

    public static function all(): array
    {
        return self::$instances;
    }
}
